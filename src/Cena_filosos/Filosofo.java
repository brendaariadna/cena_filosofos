package Cena_filosos;
//Creamos la clase Filosofo y la interfaz Runnaable para crear los hilos que se crearon en el programa
public class Filosofo implements Runnable{  
	  
	   private Thread hilo;  // Creamos un hilo
	   protected Cena cena;  // Creamos un objeto de la clase Comida
	   protected int tizq, tder;  // Creamos  unas variables que nos ayudaran a identificar si los filosofos tienes sus tenedores para poder comer
	   protected int numero;  // Creamos una variable que nos ayudar� a identificar que filosofo es el que est� en ejecuci�n 
	  
	public Filosofo (int x, Cena cena){  //Generamos el constructor con 2 parametros x ser� el num del filsofo y un objeto la clase Cena que nos dir� si el fil�sofo est� comiendo
	      this.numero= x;  // Aqu� solo especificamos que el metodo utulizar� la variable numero
	      this.cena= cena;  // Aqu� especificamos que com ser� la variable a utilizar
	      this.cena= cena;  
	      tizq= cena.getTenedorIzq(numero); //Decimos que tizq ser� el tenedor izquierdo 
	      tder= cena.getTenedorDer(numero);  // Decimos que tder ser� el tenedor derecho
	      hilo = new Thread(this);  // Creamos el nuevo hilo
	      hilo.start();  //Hilo se inicializa
	   }  
	public void PENSANDO(){  //Creamos el m�todo PENSANDO que es lo que har� en filosofo cuando no est� comiendo
	     try{  
	        System.out.println ("Filosofo "+numero+" pensando");// Mostramos qu� filosofo es el que est� pensando  
	        int espera = (int)(Math.random()*1000);  //Lo generamos aleatoriamente 
	        hilo.sleep(espera);  
	        System.out.println ("Filosofo "+numero+" tiene hambre");  
	     }catch(InterruptedException e){  
	 e.printStackTrace();  
	     }  
	}  
	public void tomar_tenedores(){  //Creamos el m�todo de tomarTenedores
	     System.out.println ("Tomando tenedores");// Imprimimos la acci�n que se est� haciendo  
	     Tenedor t1= cena.getTenedor(tizq);  //Generamos objeto para indicar que se tom� tenedor izquierdo
	     Tenedor t2= cena.getTenedor(tder);  //Generamos objeto para indicar que se tom� tenedor derecho
	     t1.usar();  //Con los objetos creados usamos el m�todo usar 
	     t2.usar();  
	}  
	public void COMIENDO(){  
		
		     try{  
		    	
		        System.out.println("Filosofo "+numero+" esta comiendo con tenedores "+tizq+" y "+tder);  //Utilizamos try para prever excepciones en el transcurso de la ejecuci�n de un programa.
		        int espera= (int)(Math.random()*700);  
		        hilo.sleep(espera);  
		        System.out.println("Filosofo "+numero+" esta satisfecho"); // Aqui solo imprimimos el fil�sofo que ya comi� 
		    	 
		     }catch(InterruptedException e){   // La excepci�n que puede ocurrir 
		  e.printStackTrace();  
		     }  
	}
	protected void bajar_tenedores(){  // M�todo para dejar los tenedores que se ocuparon
	     System.out.println("Soltando los tenedores.");  // imprimimos los tenedores que se dejan
	     Tenedor t1= cena.getTenedor(tizq);  
	     Tenedor t2= cena.getTenedor(tder);  
	     t1.dejar(); // dejamos el tenedor izquierdo 
	     t2.dejar(); // dejamos el tenedor derecho  
	}  
	public void run(){  // Hacmos el m�todo run para los hilos
	      while(true){  // Aqu� mandamos a llamar todos los m�todos que se van a invocar
	    	  PENSANDO();  
	         tomar_tenedores();  
	         COMIENDO();  
	         bajar_tenedores();  
	      }  
	}  
	}  
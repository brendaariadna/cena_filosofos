package Cena_filosos;

public class Cena{  
	 Tenedor tenedores[];  //Creamos el arreglo que nos ayudar� a definir cu�ntos tenedores vamos a tener
	 int parametro = 5;   //numero de filosofos y tenedores  
	public static void main(String args[]){  
	 System.out.println("** Cena de los Filosofos **");  
	 Cena cena = new Cena();  // Creamos objeto de la clase Cena
	 for(int i=0; i<5; i++){  //Creamos un for para los 5 fil�sofos que vamos a tener
	  Filosofo f1 = new Filosofo(i, cena);  
	 }  
	}  
	public Cena(){  
	 tenedores = new Tenedor[parametro];   //Decimos cu�ntos tenedores vamos a tener 
	 for(int i=0; i<5; i++){  // creamos el ciclo para la creaci�n de tenedores 
	  tenedores[i] = new Tenedor(i);  
	 }  
	}  
	public Tenedor getTenedor(int x){  // M�todo para obtener el tenedor 
	 return tenedores[x];  
	 }  
	public int getTenedorDer(int x){  // M�todo para obtener el tenedor derecho
	 return (x+1)%parametro;  
	 }  
	public int getTenedorIzq(int x){  // M�todo para obtener el tenedor izquierdo
	 return x;  
	 }  
	}  
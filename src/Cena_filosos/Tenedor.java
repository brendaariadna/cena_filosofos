package Cena_filosos;

public class Tenedor{  
	  
int numero;  
boolean enUso;  
   
public Tenedor(int x){  
 numero = x;  
 }  
  
synchronized public void usar(){ //Creamos el metodo usar con synchronized para que se genere la exclusi�n mutua ya que se tiene que verificar que se tengan los tenedores disponibles
      if(enUso){  //�reguntamos si el tenedor est� en uso
         System.out.println("Tenedor "+numero+" esta en uso, espera");  
      }else{  
         enUso= true;  //Si no est� en uso, hacemos que est� en uso el tenedor x
         System.out.println("Se esta usando el tenedor "+numero);  
      }  
}  
  
synchronized public void dejar(){ //Creamos el metodo dejar con synchronized para que tambi�n se genere la exclusi�n mutua
      enUso = false;  //Aqu� el tenedor avisamos que ya est� disponible
      System.out.println("Tenedor "+numero+" esta ahora disponible");  
   }  
}  
